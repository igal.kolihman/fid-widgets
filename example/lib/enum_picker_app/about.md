# EnumPicker Widget

EnumPicker allows the user to select one item from a list. 

# Use Case

You have a list of values, and you want the user to select one of them.

# Usage 

Suppose we want to select one value from a list of type TT. We can use EnumPicker as follows :


```dart
  EnumPicker<TT>( 
     [ Value1 of TT , Value1 of TT , ... ],
     value: <The initial selected value from the abobe list>,

     onChange : (TT value) {
       // This callback is called when the user selects a new value from the list.
     
     },
     render: (TT value) {
       // Allow to customize how to widget should render based on the value
       // The default callback create Text(value.toString())
     }
  )
```

# The sample

In the sample, The user can select the position and the color a box in a container.

![The initial state](images/01.png)
![User select topleft alignment](images/03.png)
![User select red color](images/05.png)

## Select the Alignment

The following EnumPicker allows the user to select the Alignment value

```dart
    EnumPicker<Alignment>(
      [ Alignment.topLeft    , Alignment.topCenter   , Alignment.topRight    ,
        Alignment.centerLeft , Alignment.center      , Alignment.centerRight ,
        Alignment.bottomLeft , Alignment.bottomCenter, Alignment.bottomRight ],
        value: this.alignment,
        onChange : (Alignment alignment) {
          this.alignment = alignment;
          setState(() => {
            alignment: this.alignment
          });
        }
    ),
```

![Select the Alignment](images/02.png)

## Select Enum Value


Suppose we have an enum that maps to colors:

```dart
enum MyColor {
  Red,
  Blue,
  Yellow
}
final colors = <MyColor, Color> {
  MyColor.Red: Colors.red,
  MyColor.Blue: Colors.blue,
  MyColor.Yellow: Colors.yellow
};
```

The following EnumPicker allows the user to select the value from MyColor enmum

```dart
    EnumPicker<MyColor>(
        MyColor.values,
        value: this.color,
        render: (value) {
          // display bar with the given color instead of the text
          return Container(
            color: colors[value],
            height: 40,
            constraints: BoxConstraints(minWidth: 100),
            //child: Text(value.toString()),
          );
        },
        onChange : (MyColor value) {
          this.color = value;
          setState(() => {
            color: this.color
          });
        }
    ),
```

![Select Enum Value](images/04.png)

Note how we use enum values method and the render callback.
