import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

enum MyColor {
  Red,
  Blue,
  Yellow
}
final colors = <MyColor, Color> {
  MyColor.Red: Colors.red,
  MyColor.Blue: Colors.blue,
  MyColor.Yellow: Colors.yellow
};

class EnumPickerApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Enum Picker Demo',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: EnumPickerHomePage());
  }
}

class EnumPickerHomePage extends StatefulWidget {
  const EnumPickerHomePage({Key key}) : super(key: key);


  @override
  State<StatefulWidget> createState() {
    return EnumPickerHomePageState();
  }
}

class EnumPickerHomePageState extends State<EnumPickerHomePage> {
  MyColor color = MyColor.Blue;
  Alignment alignment = Alignment.center;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea (
        child: Center (
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget> [
                EnumPicker<Alignment>(
                  [ Alignment.topLeft    , Alignment.topCenter   , Alignment.topRight    ,
                    Alignment.centerLeft , Alignment.center      , Alignment.centerRight ,
                    Alignment.bottomLeft , Alignment.bottomCenter, Alignment.bottomRight ],
                    value: this.alignment,
                    onChange : (Alignment alignment) {
                      this.alignment = alignment;
                      setState(() => {
                        alignment: this.alignment
                      });
                    }
                ),
                EnumPicker<MyColor>(
                    MyColor.values,
                    value: this.color,
                    render: (value) {
                      return Container(
                        color: colors[value],
                        height: 40,
                        constraints: BoxConstraints(minWidth: 100),
                        //child: Text(value.toString()),
                      );
                    },
                    onChange : (MyColor value) {
                      this.color = value;
                      setState(() => {
                        color: this.color
                      });
                    }
                ),
                Expanded(
                  child : Stack (
                    children: <Widget>[
                      Container(
                        color: Colors.black,
                        child: Align (
                            alignment: this.alignment,
                            child: Container (
                              color: colors[this.color],
                              width: 100,
                              height: 100,
                            )
                        ),
                      ),
                    ],
                  )
                )
            ]
          )
        )
      )
    );
  }
}

void main() => runApp(EnumPickerApp());