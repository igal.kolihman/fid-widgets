import 'package:example/google_login_app/pages/home_page.dart';
import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/material.dart';
import 'pages/login_page.dart';

class GoogleLoginApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shaped Image Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      routes: {
        '/home': (context) => HomePage(),
        '/login': (context) => LoginPage(),
      },

      /* 
       * Info: initial route is used instead of 'home' property 
       * Warning: When using initialRoute, don’t define a home property.
       */
      initialRoute: '/login'
    );
  }
}
