import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/foundation.dart';

class ProfileUserModuleExample extends DocumentModule {
  final String phone;
  final String description;
  final String uid;

  const ProfileUserModuleExample({
    this.phone,
    this.description,
    @required this.uid
  });

  @override
  Map<String, String> toJson() {
    return {
      'phone': phone,
      'description': description,
      'uid':uid,
    };
  }

  @override
  ProfileUserModuleExample fromFirestore(DocumentSnapshot document) {
    return new ProfileUserModuleExample(
      phone: document.data['phone'],
      description: document.data['description'],
      uid: document.data['uid'],
    );
  }

  @override
  String getID() {
    return uid;
  }
}
