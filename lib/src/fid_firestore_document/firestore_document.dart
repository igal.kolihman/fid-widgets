/*


*/

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fid_widgets/src/fid_firestore_document/modules/document_module.dart';
import 'package:flutter/material.dart';

typedef S ItemCreator<S>();

class FirestoreDocumentAPI<DOCUMENT_MODULE extends DocumentModule> {
  static final databaseReference = Firestore.instance;
  final CollectionReference collection;

  FirestoreDocumentAPI({
    @required String collectionName,
  }) : collection = databaseReference.collection(collectionName);

  Future<DocumentModule> getModuleFromFirestore(
      DocumentModule documentModuleWithID) async {
    assert(documentModuleWithID.getID() != null);
    DocumentReference documentReference = collection.document(documentModuleWithID.getID());
    
    if (documentReference == null) {
      return Future.error("no user exists");
    }

    DocumentSnapshot documentSnapshot = await documentReference.get();

    if (documentSnapshot.data != null) {
      return documentModuleWithID.fromFirestore(documentSnapshot);
    } else {
      return Future.error("no user exists");
    }
  }

  Future<DocumentModule> postModuleToFirestore(
      DocumentModule documentModule) async {
    return await collection
        .document(documentModule.getID())
        .setData(documentModule.toJson())
        .then((data) {
      return documentModule;
    });
  }

  Future<void> removeModuleFromFirestore(DocumentModule documentModule) async {
    return collection.document(documentModule.getID()).delete();
  }
}
