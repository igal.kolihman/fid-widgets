import 'package:firebase_auth/firebase_auth.dart';

class GoogleUserModule {
  final String name;
  final String email;
  final String imageUrl;
  final String uid;

  const GoogleUserModule({
    this.name,
    this.email,
    this.imageUrl,
    this.uid,
  });

  factory GoogleUserModule.fromFirebase(FirebaseUser user) {
    return new GoogleUserModule(
      uid: user.uid,
      name: user.displayName,
      email: user.email,
      imageUrl: user.photoUrl,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'email': email,
      'imageUrl': imageUrl,
      'uid': uid,
    };
  }

  bool validateFirebaseUser(FirebaseUser user) {
    return user.uid != null &&
        user.displayName != null &&
        user.photoUrl != null &&
        user.email != null;
  }
}
